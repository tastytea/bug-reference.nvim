-- SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
--
-- SPDX-License-Identifier: AGPL-3.0-only

vim.api.nvim_create_user_command('BugReference',
    require('bug-reference').open, { nargs = '?' })
