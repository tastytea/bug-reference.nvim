-- SPDX-FileCopyrightText: 2022 tastytea <tastytea@tastytea.de>
--
-- SPDX-License-Identifier: AGPL-3.0-only

local M = {}

function M.get_url(bug)
    -- called from command
    if type(bug) == 'table' then
        if bug.args == '' then
            bug = nil
        else
            bug = bug.args
        end
    end

    -- get number from word under cursor
    if bug == nil then
        bug = string.match(vim.fn.expand('<cWORD>'), '%d+$')
        if bug == nil then
            vim.notify('could not identify bug number', vim.log.levels.ERROR)
            return false
        end
        bug = bug:gsub('.*#', '')
    end

    local url_format = vim.b.bug_reference_url_format or
        vim.g.bug_reference_url_format or 'https://bugs.gentoo.org/%s'
    return url_format:format(bug)
end

function M.open(bug)
    local command
    if vim.fn.has('mac') == 1 then
        command = 'open'
    elseif vim.fn.has('win32') == 1 then
        command = 'start ""'
    elseif vim.fn.has('unix') == 1 then -- unix is true on MacOS too
        command = 'xdg-open'
    else
        vim.notify('OS not supported', vim.log.levels.ERROR)
        return false
    end

    local url = M.get_url(bug)
    if not url then
        return false
    end
    return vim.fn.jobstart({ command, url }, { detach = true }) > 0
end

return M
